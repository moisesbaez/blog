<?php
    $salt = uniqid(mt_rand(), true);
    echo $salt . " - ";

    $hashed = crypt("testbahbah", "$2y$07$" . $salt. "$");
    echo $hashed . " - ";
    
    $hashed2 = crypt("testbahbah", '$2y$07$' . $salt. '$');
    echo $hashed2 . " - ";
    
    if($hashed == $hashed2) {
        echo "yes";
    }